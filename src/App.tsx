import React from "react";
import '@/app.css';
import smallImg from '@/assets/images/5kb.png'
import bigImg from '@/assets/images/22kb.png'

const App = () => {
    return (
        <>
            <h2>webpack5-react-ts</h2>
            <img src={smallImg} alt="小于10kb的图片" />
            <img src={bigImg} alt="大于于10kb的图片" />
            <div className='smallImg'></div> {/* 小图片背景容器 */}
            <div className='bigImg'></div> {/* 大图片背景容器 */}
        </>
    )
}

export default App;